<?php


session_start();
$result = false;

$errors = array();
if(isset($_POST['submit'])) {
    
    require_once "dbconfig.php";
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $name = strip_tags($_POST['name']);
        $email = strip_tags($_POST['email']);
        $phone = strip_tags($_POST['phone_number']);
        $message = strip_tags($_POST['message']);
        
        if (validate_user()) {
            $result = create_user();
           send_email();
        
        }

//        redirect_to_form();
        
        
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}
    

function create_user(){
    
    global $conn, $name, $email, $phone, $message;
    
    $data = [
        'name' => $name,
        'email' => $email,
        'phone' => $phone,
        'message' => $message,
    ];
    try {
        
        $sql = "INSERT INTO users (name, email, phone, message) VALUES (:name, :email, :phone, :message)";
        $stmt = $conn->prepare($sql);
        $stmt->execute($data);
        if($stmt === false){
            $_SESSION['msg'] = 'Error inserting the ysers.';
            return false;
        }else{
            $_SESSION['msg'] = "The new User $name is created";
            return true;
        }
    } catch (Exception $e){
        throw $e;
    }

    
  
}

function redirect_to_form(){
    header("Location: http://localhost:IEEE-Form");
    exit() or die();
};

function validate_user(){
    
    global $errors, $name, $email, $phone, $message;
    
    
    if(strlen($name) == 0) {
        $errors['name'] = 'Name is required';
    }elseif (strlen(trim($name)) < 5){
        $errors['name'] = 'Name should be more than 5 letters';
    }else{
        $name = filter_var($name,FILTER_SANITIZE_STRING);
        unset($errors['name']);
    }
    

    
    if(strlen($email) == 0) {
        $errors['email'] = 'Email is required';
    } elseif (!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $errors['email'] = 'Email is not valid';
    }else{
        $email = filter_var($email,FILTER_SANITIZE_STRING);
        unset($errors['email']);
    }
    
    if(strlen($phone) == 0) {
        $errors['phone'] = 'Phone is required';
    } else{
        $phone = filter_var($phone,FILTER_SANITIZE_NUMBER_INT);
        unset($errors['phone']);
    }
    
    
     if(strlen($message) == 0) {
        $errors['message'] = 'Message is required';
    }else{
        $message = filter_var($message,FILTER_SANITIZE_STRING);
         unset($errors['message']);
    }
    
    
    if(count($errors)){
        return false;
    }else{
        return true;
    }
}

function send_email(){
    global $email;
    error_reporting(E_ALL|E_STRICT);
    ini_set('display_errors', 1);
    ini_set('sendmail_from', 'hodamosaad0@gmail.com');
//the subject
    $sub = "Your subject";
//the message
    $msg = "Your message";
//send email
    $res = mail($email,$sub,$msg);
    if (!$res){
        //
    }
    
    
    
    
    
}
