
var  nameErr = document.getElementById('nameErr'),
    phoneErr = document.getElementById('phoneErr'),
    emailErr = document.getElementById('emailErr'),
    messageErr = document.getElementById('messageErr');

var nameIn = $('#name'),
    phoneIn = $('#phone_number'),
    emailIn = $('#email'),
    messageIn = $('#message');


var flag = 0;


nameIn.blur(function () {
    validate_name()
});
phoneIn.blur(function () {
    validate_phone();
});

emailIn.blur(function () {
    validate_email();
});

messageIn.blur(function () {
   validate_message()
});




function check_empty(input){
    if (input.split(' ').join('').length === 0){
        flag = 1;
        return true
    }
    return false
}



function validate_name(){
    flag = 0;
    var name = $('#name').val();

    let result = /^[a-zA-Z ]+$/.test(name);
    if(check_empty(name)) {
        nameIn.css({'border-bottom': '1px solid red'});
        nameErr.innerHTML = "We need to know your name!";
    }
    else if(!result){
        nameIn.css({'border-bottom': '1px solid red'});
        nameErr.innerHTML = "Name shouldn't contain numbers";
        flag = 1;
    }
    else if (name.trim().length < 5){
        nameErr.innerHTML = 'Name should be more than 5 letters.';
        nameIn.css({'border-bottom': '1px solid red'});
        flag = 1;
    }
    else {
        nameIn.css({
            'border': 'none',
            'border-bottom': '1px solid green'
        });
        nameErr.innerHTML = '';
    }
}
function validate_phone() {
    flag = 0;
    var phone_number = $('#phone_number').val();
    let res = /^[0-9 ]+$/.test(phone_number);


    if (check_empty(phone_number)){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = 'We need to know your phone number!';
    }
    else if (!res){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = "Phone number shouldn\'t contain letters or spaces or symbols";
        flag = 1;
    }
    else if (phone_number.trim().length != 11){
        phoneIn.css({'border-bottom': '1px solid red'});
        phoneErr.innerHTML = "Phone number should be 11 number";
        flag = 1;
    }
    else {

        phoneIn.css({
            'border': 'none',
            'border-bottom': '1px solid green'});
        phoneErr.innerHTML = "";
    }
}
function validate_email() {
    flag = 0;
    var email = $('#email').val();
    var findAt = email.search(/@/i);
    var findCom = email.search(/.com/i);

    if (check_empty(email)){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "We need to know your e-mail address!";
        flag = 1;
    } else if (findAt == -1){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "Email should contain @";
        flag = 1;
    } else if (findCom == -1){
        emailIn.css({'border-bottom': '1px solid red'});
        emailErr.innerHTML = "Email should contain .com";
        flag = 1;
    } else {
        emailIn.css({
            'border': 'none',
            'border-bottom': '1px solid green'});
            emailErr.innerHTML = "";
    }
}

function validate_message() {
    flag = 0;
    var message = $('#message').val();

    if(check_empty(message)) {
        messageIn.css({'border-bottom': '1px solid red'});
        messageErr.innerHTML = "We need to know your Message!";
        flag = 1;
    }
    else if (message.trim().length < 10){
        messageErr.innerHTML = 'Message should be more than 10 letters.';
        messageIn.css({'border-bottom': '1px solid red'});
        flag = 1;
    }
    else {
        messageIn.css({
            'border': 'none',
            'border-bottom': '1px solid green'
        });
        messageErr.innerHTML = '';
    }
}

var subm = $('#subm');

subm.click(function (x)  {
    validate_name();
    validate_email();
    validate_phone();
    validate_message();
    if (flag === 1){
        x.preventDefault()
    }
});



