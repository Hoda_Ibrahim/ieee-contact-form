<?php include "process/process.php";?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/materialize.min.css">
    <link rel="stylesheet"  href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="hel">
    <section>
        <div class="fSec seven">

            <h3 class="mz1 text-center">Contact Form</h3>
            <div class="ri1">
                <h3 class="e1 text-center"></h3>
                <span class="daimond"></span>
                <p class="d1">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae deserunt enim esse et explicabo fugiat ipsam iste iure, libero magni maxime molestiae obcaecati officiis porro quo ratione recusandae totam ullam.
                </p>
            </div>
        </div>
    </section>
    <?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])) : ?>
        <div class="message">
            <?= $_SESSION['msg']; ?>
        </div>
    <?php
        // remove all session variables
        session_unset();

        // destroy the session
        session_destroy();
        ?>
    <?php endif; ?>
    

    
    
    <form id="test" action="" method="post">
        <div class="row seven" id="first">

            <div class="input-field col s12">
                <i class="material-icons prefix">account_circle</i>
                <input <?php if(isset($name)) :?> value="<?= $name ?>" <?php endif;?>  name="name" id="name" type="text" class="validate">
                <label for="name">Name </label>
                <?php if(isset($errors['name'])): ?>
                    <span id="nameErr">
                        <?= $errors['name'] ?>
                    </span>
                <?php endif; ?>
                <span id="nameErr"></span>
            </div>


            <div class="input-field col s12">
                <i class="material-icons prefix">phone</i>
                <input <?php if(isset($phone)) :?> value="<?= $phone ?>" <?php endif;?>  name="phone_number" id="phone_number" type="tel" class="validate">
                <label for="phone_number">Phone Number</label>
                <?php if(isset($errors['phone'])): ?>
                    <span id="phoneErr">
                        <?= $errors['phone'] ?>
                    </span>
                <?php endif; ?>
                <span id="phoneErr"></span>
            </div>

            <div class="input-field col s12">
                <i class="material-icons prefix">email</i>
                <input <?php if(isset($email)) :?> value="<?= $email ?>" <?php endif;?>  name="email" id="email" type="text" class="validate">
                <label for="email">Email</label>
                <?php if(isset($errors['email'])): ?>
                    <span id="emailErr">
                        <?= $errors['email'] ?>
                    </span>
                <?php endif; ?>
                <span id="emailErr"></span>
            </div>

            <div class="input-field col s12">
                <i class="material-icons prefix">textsms</i>
                <textarea  name="message" id="message" type="text"
                           class="validate"><?php if(isset($message)) :?> <?= $message ?> <?php endif;?></textarea>
                <label for="message">Message</label>
                <?php if(isset($errors['message'])): ?>
                    <span id="messageErr">
                        <?= $errors['message'] ?>
                    </span>
                <?php endif; ?>
                <span id="messageErr"></span>
            </div>
            

            <button id="subm" class="btn waves-effect waves-light" type="submit" name="submit"> Send
                <i class="material-icons right">send</i>
            </button>
        </div>

    </form>
</div>

<script src="js/materialize.min.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>